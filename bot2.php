#!/usr/bin/php
<?PHP

chdir ( '/data/project/listeria' ) ;

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // |E_ALL
ini_set('memory_limit','4000M');


$blocked_namespaces = array ( // Wiki => Comma-separated list of namespace IDs
	'dewiki' => '0' 
) ;


require_once ( '/data/project/listeria/public_html/php/common.php' ) ;
require_once ( '/data/project/listeria/shared.inc' ) ;
require_once ( '/data/project/listeria/public_html/php/wikiquery.php' ) ;
require_once ( '/data/project/listeria/public_html/php/wikidata.php' ) ;


function updateWikis () {
	global $tool_db ;
	$q = 'Q19860885' ;
	$il = new WikidataItemList() ;
	$il->loadItems ( array ( $q ) ) ;

	$o = $il->getItem($q) ;
	foreach ( $o->j->sitelinks AS $wiki => $v ) {
		$page = $v->title ;
		$sql = "INSERT IGNORE INTO wikis (name) VALUES ('".$tool_db->real_escape_string($wiki)."')" ;
		if(!$result = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']');
	}
}


function getTimestamp($x) {
	return date ( 'YmdHis' , time()-$x*60*60 ) ;
}

function resetOldRunning () {
	global $tool_db ;
	$old = 1 ; // Hours
	$sql = "UPDATE pagestatus SET `status`='TIMEOUT',message='' WHERE `status`='RUNNING' AND timestamp<='" . getTimestamp($old) . "'" ;
	if(!$result = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']'."\n$sql\n\n");
}



function updatePagesForWiki ( $wiki ) {
	global $tool_db , $blocked_namespaces ;
	$server_parts = explode ( '.' , getWebserverForWiki ( $wiki ) ) ;
	$lang = $server_parts[0] ;
	$project = $server_parts[1] ;
	if ( $project == 'wikidata' ) $lang = $project ;

	$t1 = 'Wikidata list' ;
	$t2 = 'Wikidata list end' ;
	$wil = new WikidataItemList($lang) ;
	$wil->loadItems ( array ( 'Q19860885' , 'Q19860887' ) ) ;

	$sl = $wil->getItem('Q19860885')->getSitelink($wiki) ;
	if ( isset($sl) ) $t1 = preg_replace ( '/^[^:]+:/' , '' , $sl ) ;

	$sl = $wil->getItem('Q19860887')->getSitelink($wiki) ;
	if ( isset($sl) ) $t2 = preg_replace ( '/^[^:]+:/' , '' , $sl ) ;

	$t1 = str_replace ( ' ' , '_' , $t1 ) ;
	$t2 = str_replace ( ' ' , '_' , $t2 ) ;
	
	if ( $t1 == '' or $t2 == '' ) {
		print "Wiki $wiki is lacking templates\n" ;
		return ;
	}

	// Get wiki ID
	$wiki_id = 0 ;
	$sql = "SELECT id FROM wikis WHERE name='".$tool_db->real_escape_string($wiki)."'" ;
	if(!$result = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']'."\n$sql\n\n");
	while($o = $result->fetch_object()) $wiki_id = $o->id ;
	if ( $wiki_id == 0 ) {
		print "No such wiki $wiki\n" ;
		return ;
	}

	// Mark Listeria pages for this wiki as "CHECKING"
	$sql = "UPDATE pagestatus SET `status`='CHECKING' WHERE wiki=$wiki_id" ;
	if(!$result = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']'."\n$sql\n\n");

	$wq = new WikiQuery ( $lang , $project ) ;
	$ns = $wq->get_namespaces() ;
	$ts = getTimestamp(0) ;

	// Add or confirm Listeria pages
	$db = openDB ( $lang , $project ) ;
	$sql = "select page.* from page,templatelinks t1,templatelinks t2 where page_id=t1.tl_from and t1.tl_title='$t1' and page_id=t2.tl_from and t2.tl_title='$t2' and t1.tl_namespace=10 and t2.tl_namespace=10" ;
	if ( isset($blocked_namespaces[$wiki]) ) $sql .= " AND page_namespace NOT IN (" . $blocked_namespaces[$wiki] . ")" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$page = $o->page_title ;
		if ( $o->page_namespace > 0 ) $page = $ns[$o->page_namespace] . ':' . $page ;
		$sql = "INSERT INTO pagestatus (wiki,page,`status`,`message`,`timestamp`) VALUES ($wiki_id,'".$tool_db->real_escape_string($page)."','WAITING','','$ts')" ;
		$sql .= " ON DUPLICATE KEY UPDATE `status`='WAITING',`message`='',`timestamp`='$ts'" ;
		if(!$r2 = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']'."\n$sql\n\n");
	}
	
	// Remove pages from pagestatus that no longer are Listeria
	$sql = "DELETE FROM pagestatus WHERE `status`='CHECKING' AND wiki=$wiki_id" ;
	if(!$result = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']'."\n$sql\n\n");
}

function updatePagesForAllWikis () {
	global $tool_db ;
	$sql = "SELECT * FROM wikis" ;
	if(!$result = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']'."\n$sql\n\n");
	while($o = $result->fetch_object()){
		updatePagesForWiki ( $o->name ) ;
	}
}

function resetWikis () {
	global $tool_db ;
	$ts = getTimestamp(0) ;
	$sql = "UPDATE wikis SET `status`='ACTIVE',timestamp='$ts' WHERE `status`!='IGNORE'" ;
	if(!$result = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']'."\n$sql\n\n");
}

function resetAllPages () {
	global $tool_db ;
	$ts = getTimestamp(0) ;
	$sql = "UPDATE pagestatus SET `status`='WAITING',`message`='',timestamp='$ts' WHERE `status`!='RUNNING'" ;
	if(!$result = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']'."\n$sql\n\n");
	// TODO update pages
}

function updateSingleRandomPage () {
	$tool_db = openToolDB ( 'listeria_bot' ) ;
	$tool_db->set_charset("utf8") ;

	$p = '' ;
	$sql = "SELECT pagestatus.id AS page_id,page,wikis.name AS wiki FROM pagestatus,wikis WHERE wikis.id=pagestatus.wiki AND pagestatus.status='WAITING' ORDER BY rand() LIMIT 1" ;
	if(!$result = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']'."\n$sql\n\n");
	while($o = $result->fetch_object()) $p = $o ;
	if ( $p == '' ) return ; // Nothing left to do

	$ts = getTimestamp(0) ;
	$sql = "UPDATE pagestatus SET `status`='RUNNING',`message`='',timestamp='$ts' WHERE id={$p->page_id}" ;
	if(!$result = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']'."\n$sql\n\n");

	$server_parts = explode ( '.' , getWebserverForWiki ( $p->wiki ) ) ;
	$lang = $server_parts[0] ;
	$project = $server_parts[1] ;
	if ( $project == 'wikidata' ) $lang = $project ;

	$l = new Listeria ( $lang ) ;
	$success = $l->updatePage ( $p->page ) ;
	$status = isset($l->error) ? 'FAIL' : 'OK' ;
	$msg = isset($l->error) ? $l->error : $l->status_message ;
	

	$query_wdq = isset($l->wdq) ? $l->wdq : '' ;
	$query_sparql = isset($l->sparql) ? $l->sparql : '' ;
	if ( trim($query_sparql) != '' ) $query_wdq = '' ; // SPARQL overrides WDQ
	$ts = getTimestamp(0) ;
	$sql = "UPDATE pagestatus SET `status`='" . $tool_db->real_escape_string($status) . "',`message`='" . $tool_db->real_escape_string($msg) . "',timestamp='$ts'" ;
	$sql .= ",query_wdq='" . $tool_db->real_escape_string($query_wdq) . "'" ;
	$sql .= ",query_sparql='" . $tool_db->real_escape_string($query_sparql) . "'" ;
	$sql .= " WHERE id={$p->page_id}" ;
	if(!$result = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']'."\n$sql\n\n");
}


$tool_db = openToolDB ( 'listeria_bot' ) ;
$tool_db->set_charset("utf8") ;
resetOldRunning() ;

$cmd = $argv[1] ;
if ( $cmd == 'update_wikis' ) {
	updateWikis () ;
	resetWikis() ;
	resetAllPages() ;
	updatePagesForAllWikis() ;
} else if ( $cmd == 'update_pages' ) {
	$num = 10 ;
	if ( isset ( $argv[2] ) ) $num = $argv[2] * 1 ;
	for ( $n = 0 ; $n < $num ; $n++ ) updateSingleRandomPage() ;
}



/*
if ( count ( $argv ) != 2 ) {
#	print "USAGE: " . $argv[0] . " LANGUAGE\n" ;
	$wiki = getNextWiki() ;
	if ( $wiki == '' ) exit ( 0 ) ; # All running
} else {
	$wiki = strtolower ( trim ( $argv[1] ) ) ;
}

$server_parts = explode ( '.' , getWebserverForWiki ( $wiki ) ) ;
$lang = $server_parts[0] ;
$project = $server_parts[1] ;
if ( $project == 'wikidata' ) $lang = $project ;
#print "$wiki: $lang.$project\n" ;

setWikiStatus ( $wiki , 'RUNNING' ) ;
$sql = "SELECT * FROM wikis WHERE name='$wiki'" ;
if(!$result = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']');
while($o = $result->fetch_object()) $wiki_id = $o->id ;
if ( !isset($wiki_id) ) die ( "UNKNOWN WIKI '$wiki' IN DATABASE\n" ) ;
$sql = "DELETE FROM pagestatus WHERE wiki='$wiki_id'" ;
if(!$result = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']');

$wq = new WikiQuery ( $lang , $project ) ;
$ns = $wq->get_namespaces() ;


$t1 = 'Wikidata list' ;
$t2 = 'Wikidata list end' ;
$wil = new WikidataItemList($lang) ;
$wil->loadItems ( array ( 'Q19860885' , 'Q19860887' ) ) ;

$sl = $wil->getItem('Q19860885')->getSitelink($wiki) ;
if ( isset($sl) ) $t1 = preg_replace ( '/^[^:]+:/' , '' , $sl ) ;

$sl = $wil->getItem('Q19860887')->getSitelink($wiki) ;
if ( isset($sl) ) $t2 = preg_replace ( '/^[^:]+:/' , '' , $sl ) ;

$t1 = str_replace ( ' ' , '_' , $t1 ) ;
$t2 = str_replace ( ' ' , '_' , $t2 ) ;


$db = openDB ( $lang , $project ) ;

$t1 = $db->real_escape_string ( $t1 ) ;
$t2 = $db->real_escape_string ( $t2 ) ;

$sql = "select page.* from page,templatelinks t1,templatelinks t2 where page_id=t1.tl_from and t1.tl_title='$t1' and page_id=t2.tl_from and t2.tl_title='$t2' and t1.tl_namespace=10 and t2.tl_namespace=10" ;
if ( $wiki == 'dewiki' ) $sql .= " AND page_namespace!=0" ;
$sql .= " ORDER BY rand()" ; // In case of 'broken' pages, don't always process the same ones first
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$page = $o->page_title ;
	if ( $o->page_namespace > 0 ) $page = $ns[$o->page_namespace] . ':' . $page ;

	$page_db = $tool_db->real_escape_string($page) ;
	$sql = "REPLACE INTO pagestatus (wiki,page,status,timestamp) VALUES ('$wiki_id','$page_db','RUNNING','".getTimestamp(0)."')" ;
	if(!$result2 = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']');
#	print "$wiki:$page\n" ;
	
	$l = new Listeria ( $lang ) ;
	$success = $l->updatePage ( $page ) ;
	$tool_db = openToolDB ( 'listeria_bot' ) ;
	$tool_db->set_charset("utf8") ;
	$status = isset($l->error) ? $l->error : 'OK' ;
	$sql = "REPLACE INTO pagestatus (wiki,page,status,timestamp) VALUES ('$wiki_id','$page_db','".$tool_db->real_escape_string($status)."','".getTimestamp(0)."')" ;
	if(!$result2 = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']');
}

setWikiStatus ( $wiki , 'DONE' ) ;
*/

?>
