<?PHP

//error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR); ini_set('display_errors', 'On');
require_once ( 'php/common.php' ) ;


function updateList ( $list ) {
	global $wdq_internal_url , $db , $out ;
	$sql = "SELECT * FROM list WHERE id=$list LIMIT 1" ;
	$ld = (object) array() ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n\n$sql\n");
	while($o = $result->fetch_object()) {
		$o->config = json_decode ( $o->config ) ;
		$ld = $o ;
	}
	if ( !isset($ld->id) ) die ( "Wrong list ID: $list" ) ;
	
	$url = "$wdq_internal_url?q=" . urlencode($ld->wdq) ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	
	$sql = "UPDATE list SET last_update='" . $out['ts'] . "',items='," . implode(',',$j->items) . ",',number_of_items=".count($j->items)." WHERE id=$list" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n\n$sql\n");
}

function listLists ( $sql ) {
	global $out , $db ;
	if ( !isset($db) ) $db = openToolDB ( 'listeria_p' ) ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n\n$sql\n");
	$out['data'] = array() ;
	while($o = $result->fetch_object()) {
		$out['data'][] = $o ;
	}
}

header('Content-type: application/json; charset=UTF-8');

$query = get_request ( 'query' , '' ) ;
$out = array ( 'status' => 'OK' , 'ts' => date('c') ) ;

if ( $query == 'get_list' ) {
	$list = get_request ( 'list' , 0 ) * 1 ;
	$sql = "SELECT * FROM list WHERE id=$list LIMIT 1" ;
	$db = openToolDB ( 'listeria_p' ) ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n\n$sql\n");
	while($o = $result->fetch_object()) {
		$o->config = json_decode ( $o->config ) ;
		$out['data'] = $o ;
	}

} else if ( $query == 'update_list' ) {

	$list = get_request ( 'list' , 0 ) * 1 ;
	$db = openToolDB ( 'listeria_p' ) ;
	updateList ( $list ) ;


} else if ( $query == 'get_last_lists' ) {

	$limit = get_request ( 'limit' , 10 ) * 1 ;
	listLists ( "SELECT * FROM list ORDER BY created DESC LIMIT $limit" ) ;

} else if ( $query == 'search' ) {

	$text = get_request ( 'text' , '' ) ;
	$limit = get_request ( 'limit' , 10 ) * 1 ;
	
	$db = openToolDB ( 'listeria_p' ) ;
	$text = trim ( preg_replace ( '/\s+/' , ' ' , $text ) ) ;
	if ( $text != '' ) {
		$t = explode ( ' ' , $text ) ;
		foreach ( $t AS $k => $v ) {
			$sql = array() ;
			if ( preg_match ( '/^[qQ](\d+)$/' , $v , $m ) ) {
				$sql[] = "(items LIKE '%," . $m[1] . ",%')" ;
			} else {
				$sql[] = "(name LIKE '%" . $db->real_escape_string ( $v ) . "%')" ;
				$sql[] = "(owner LIKE '%" . $db->real_escape_string ( $v ) . "%')" ;
			}
			$t[$k] = "(" . implode ( ' OR ' , $sql ) . ")" ;
		}
		$sql = "SELECT * FROM list WHERE (" . implode ( ' AND ' , $t )  . ") LIMIT $limit" ;
		$out['sql'] = $sql ;
		listLists ( $sql ) ;
	} else $out['data'] = array() ;

} else if ( $query == 'get_user_lists' ) {

	$db = openToolDB ( 'listeria_p' ) ;
	$limit = get_request ( 'limit' , 10 ) * 1 ;
	$user = $db->real_escape_string ( get_request ( 'user' , '' ) ) ;
	listLists ( "SELECT * FROM list WHERE owner='$user' ORDER BY created DESC" ) ;


} else if ( $query == 'set_list' ) {
	
	$list = json_decode ( get_request ( 'list' , '{}' ) ) ;
	if ( !isset($list->owner) or $list->owner == '' ) {
		$out['status'] = 'No owner' ;
	} else {
		$db = openToolDB ( 'listeria_p' ) ;
		$name = $db->real_escape_string ( $list->name ) ;
		$wdq = $db->real_escape_string ( $list->wdq ) ;
		$config = $db->real_escape_string ( json_encode($list->config) ) ;
		$owner = $db->real_escape_string ( $list->owner ) ;
		if ( isset($list->id) ) {
			$sql = "UPDATE list SET name='$name',wdq='$wdq',config='$config',created='".$out['ts']."' WHERE id=".($list->id*1)." AND owner='$owner'" ;
		} else {
			$sql = "INSERT INTO list  (name,wdq,config,owner,created) VALUES ('$name','$wdq','$config','$owner','".$out['ts']."')" ;
		}
		$out['sql'] = $sql ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n\n$sql\n");
		if ( !isset($list->id) ) {
			$list->id = $db->insert_id ;
			updateList ( $list->id ) ; // Initial update
		}
		$out['id'] = $list->id ;
	}


/*
} else if ( $query == 'load_labels' ) {

	$items = get_request ( 'items' , '' ) ;
	$items = preg_replace ( '/[^0-9,]/' , '' , $items ) ;
	$sql = "SELECT term_entity_id,term_language,term_text FROM wb_terms WHERE term_entity_type='item' AND term_type='label' AND term_entity_id IN ($items)" ;
	$db = openDB ( 'wikidata' , 'wikidata' ) ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n\n$sql\n");
	while($o = $result->fetch_object()) {
//		$out['data'][] = array ( 'Q'.$o->term_entity_id , $o->term_language , $o->term_text ) ;
		$out['data']['Q'.$o->term_entity_id][$o->term_language] = $o->term_text ;
	}
*/

}

print json_encode ( $out ) ;

?>