#!/usr/bin/php
<?PHP

require_once ( 'public_html/php/common.php' ) ;

$wdq2ids = array() ;

$db = openToolDB ( 'listeria_p' ) ;
$sql = "SELECT wdq,group_concat(id) AS ids FROM list GROUP BY wdq" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n\n$sql\n");
while($o = $result->fetch_object()) {
	$wdq2ids[$o->wdq] = $o->ids ;
}

foreach ( $wdq2ids AS $wdq => $ids ) {
	$url = "$wdq_internal_url?q=" . urlencode($wdq) ;
	$j = @file_get_contents ( $url ) ;
	if ( $j === null or $j == '' or !isset($j) ) {
		print "Error while running $wdq\n" ;
		continue ;
	}
	$j = json_decode ( $j ) ;
	$items = ',' . implode ( ',' , $j->items ) . ',' ;

	if ( !$db->ping() ) $db = openToolDB ( 'listeria_p' ) ; // Paranoia

	$sql = "UPDATE list SET last_update='" . date('c') . "',items='$items',number_of_items=".count($j->items)." WHERE id IN ($ids)" ;
	if(!$result = $db->query($sql)) {
		print 'There was an error running the query [' . $db->error . ']'."\n\n$sql\n" ;
		continue ;
	}
}

?>